// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MultiplayerExampleGameMode.generated.h"

UCLASS(minimalapi)
class AMultiplayerExampleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMultiplayerExampleGameMode();
};



