# MultiplayerExample

Developed with Unreal Engine 4.25 and JetBrains Rider https://www.jetbrains.com/lp/rider-unreal/

This is based on the tutorial from https://docs.unrealengine.com/en-US/Gameplay/Networking/QuickStart/index.html,
with some code correction to work against Unreal Engine 4.25
